package dev.balakumar.dummyresponsews;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class DummyResponseWsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DummyResponseWsApplication.class, args);
    }

}
