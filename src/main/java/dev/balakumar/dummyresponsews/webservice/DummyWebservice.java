package dev.balakumar.dummyresponsews.webservice;


import lombok.extern.slf4j.Slf4j;
import org.springframework.core.io.ClassPathResource;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StreamUtils;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.nio.charset.Charset;
@Slf4j
@RestController
public class DummyWebservice {



    @GetMapping("api/v1/product/{id}")
    ResponseEntity<String> getProduct(@PathVariable String id) {
        return ResponseEntity.ok("{ \"productId\" : " + id + ", \"basePrice\": 100.10 }");
    }


    @PostMapping("api/v1/tax")
    ResponseEntity<String> getTaxCalculated() {
        return ResponseEntity.ok("{\"taxAmount\": 10.10 } ");
    }

    @PostMapping("ordersystem")
    ResponseEntity<String> getOrder(@RequestBody String request) throws IOException {
        log.debug("Received request : {}", request);
        String msg = StreamUtils.copyToString( new ClassPathResource("soapResponse.xml").getInputStream(), Charset.defaultCharset()  );
        return ResponseEntity.ok().contentType(MediaType.TEXT_XML).body(msg);
    }


}
